https://archive.ics.uci.edu/ml/datasets/Abalone










Setting up this library is much easier using intelliJ than eclipse. (https://www.jetbrains.com/idea/?fromMenu)

Convolutional Neural Networks (we will be using this to identify images) http://deeplearning4j.org/convolutionalnets

# Instruction for maven: #
1. download it here: http://ftp.ps.pl/pub/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip
2. unzip it in some directory
3. follow https://maven.apache.org/install.html
  this is how I did it for Windows: 
      * press windows key + pause key
      * go to advanced settings -> environmental variables
      * in the upper window add a JAVA_HOME variable and make it the path to your jdk (C:\Program Files\Java\jdk1.8.0_91 for      me)
      * in the lower window find the PATH variable, edit it and add the bin folder in your maven folder at the end (for me it's C:\apache-maven-3.3.9\bin)
      * open command line (win+R -> cmd)
      * write mvn -v, it should show some data about maven.
 
If you followed the above points maven should be installed.

# Running examples #

Now, you will need your git program and intelliJ.
1. First, pull this git repository. 
2. Then, in intellij go to file->open and find DL4J.
3. After choosing it you should be able to see the project.
4. Now you can try to find and build LenetMnistExample in src/main/java/ord.deeplearning4j.examples/convolution/, and run it.
 You should see something like:
o.d.o.l.ScoreIterationListener - Score at iteration 0 is 2.2650271633081136
start appearing after a while. 
Congratulations - that means a convolutional neural net started learning from the MNIST database of number images!